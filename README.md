# README #

Angular 1.5 - Redux - Angular material startup project.

Technologies:

- Angular 1.5.8
- Redux
_ Angular Material
- Angular router 1.5.8
- Babel (es6)
- Webpack (with dev server)

Installation:

- Install node.js: https://nodejs.org/en/download/
- Clone the repo.
- Run 'npm install' to install all the dependencies
- Run 'node index.js' to startup the web server
- App runs at localhost:3001
- BE runs at localhost:3000/api

Next steps:
- Use ui-router (angular-ui-router)
- Add karma, mocha, chai for unit testing
- Add gulp for automated tasks
- proxy BE through app server at some route (/api)
