require('./servers/backend');
var config = require("./webpack.config.js"),
    webpack = require('webpack'),
    WebpackDevServer = require('webpack-dev-server');

config.entry.app.unshift("webpack-dev-server/client?http://localhost:3001/");
var compiler = webpack(config);
var server = new WebpackDevServer(compiler, {hot: true});
server.listen(3001);

module.exports = server;

