var express = require('express'),
    router = express.Router();

module.exports = function(app) {

    app.use(function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
        next();
    });
    // isAlive
    router.get('/', function(req, res) {
        res.json({
            message: 'API ok!'
        });
    });

    router.get('/videos', function(req, res) {
        const buildVideo = (author, title, id) => {
            return {
                title,
                author,
                id
            };
        }

        const videos = [buildVideo('Bireli', 'You make me feel so young', 'UIKOUnZjgBE'),
            buildVideo('Coltrane', 'Giant Steps', '30FTr6G53VU'),
            buildVideo('Pat Martino', 'Sunny', 'sD_9dTwmidw'),
            buildVideo('Bireli', 'Nuits', 'WYrbn0ohDRM')
        ];

        res.json({
            videos
        });
    });
    app.use('/api', router);
};
