var express = require('express'),
    bodyParser = require('body-parser'),
    path = require('path');

// create express server
var app = express();
app.use(bodyParser.json());
// serve static folder

// create routes
require('./routes')(app);
// listen
app.listen(3000);

module.exports = app;
