var path = require("path");
var webpack = require('webpack');

module.exports = {
    entry: {
        app: ["./src/entry/app.js"],
        vendor: [
                    'angular', 
                    'angular-material', 
                    'ng-redux', 
                    'redux-logger', 
                    'redux-promise-middleware', 
                    'redux-thunk']
    },
    output: {
        path: path.resolve(__dirname, "build"),
        publicPath: "/assets/",
        filename: "bundle.js"
    },
    module: {
        loaders: [{
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel', // 'babel-loader' is also a valid name to reference
            query: {
                presets: ['es2015']
            }
        }, {
            test: /\.html$/,
            loader: 'raw'
        }, {
            test: /\.css$/,
            loaders: [
                'style-loader',
                'css-loader'
            ]
        }, {
            test: /\.styl$/,
            loaders: [
                'style-loader',
                'css-loader',
                'stylus-loader'
            ]
        }, ]
    },
    resolve: {
        root: path.resolve(__dirname),
        alias: {
            app: path.resolve(__dirname) + '/src',
            assets: path.resolve(__dirname) + '/assets'
        }
    },
    target: "web",
    devtool: 'source-map',
  plugins: [
    new webpack.optimize.CommonsChunkPlugin(/* chunkName= */"vendor", /* filename= */"vendor.bundle.js")
  ]
};
