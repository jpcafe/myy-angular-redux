import Artist from './artist-class';

export default class ArtistHelper {
    static buildList(videos) {
        const artists = [];
        videos.forEach(v => {
            if (!this.hasArtist(v.author, artists)) {
                artists.push(new Artist(v.author, true));
            }
        });
        return artists;
    }

    static hasArtist(name, artists) {
        return !!this.getArtist(name, artists);
    }

    static getArtist(name, artists = []) {
        return artists.find(a => a.name === name);
    }

    static getArtistIndex(name, artists) {
        return (artists || []).findIndex(a => a.name === name);
    }

    static selectArtist(name, artists) {
        const index = this.getArtistIndex(name, artists);
        return [...artists.slice(0, index), new Artist(name, !artists[index].selected), ...artists.slice(index + 1)];
    }
}
