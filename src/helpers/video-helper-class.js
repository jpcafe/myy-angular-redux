import Video from './video-class';

export default class VideoHelper {
    static buildList(data = []) {
        const videos = [];
        data.forEach(v => videos.push(new Video(v.author, v.title, v.id)));
        return videos;
    }

    static filterByArtist(videos = [], artists = []) {
        const reducedArtists = artists.filter(a => a.selected === true).map(a => a.name);
        return videos.filter(v => !!reducedArtists.find(a => a === v.author));
    }

    static filterByTitle(videos = [], text = '') {
        return videos.filter(v => v.title.toLowerCase().includes(text.toLowerCase()));
    }

    static filter(videos = [], artists = [], text = '') {
      return this.filterByTitle(this.filterByArtist(videos, artists), text);
    }
}
