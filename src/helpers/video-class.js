export default class Video {
    constructor (author = '', title = '', id = null) {
        this.author = author;
        this.title = title;
        this.id = id;
        this.url = `https://www.youtube.com/embed/${id}`;
    }
}