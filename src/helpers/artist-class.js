export default class Artist {
    constructor(name, selected = true) {
        this.name = name;
        this.selected = selected;
    }
    static from(artist) {
        return new Artist(artist.name, artist.selected);
    }
}
