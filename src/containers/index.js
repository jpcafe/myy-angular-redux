import HomeModule from './home/home';
import angular from 'angular';

export default angular.module('app.containers', [
    HomeModule.name
]);