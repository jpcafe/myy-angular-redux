import angular from 'angular';
import component from './home.component';

export default angular.module('app.containers.home', [])
.component('homeContainer', component);
