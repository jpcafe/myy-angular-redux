import * as HomeActions from 'app/actions/home-actions';

export default class HomeController {
    constructor($ngRedux) {
        this.unsubscribe = $ngRedux.connect(
                this.mapStateToThis,
                HomeActions)
            (this);

        this.loadVideos();
    }

    mapStateToThis(state) {
        return {
            videos: state.home.filteredVideos,
            artists: angular.copy(state.home.artists),
            selectedVideo: state.home.selectedVideo,
            searchText: state.home.searchText
        }
    }

    $onDestroy() {
        this.unsubscribe();
    }
}
