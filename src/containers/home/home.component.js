import template from './home.html';
import controller from './home.controller';

const homeComponent = {
    template,
    controller,
    controllerAs: 'vm'
};

export default homeComponent;