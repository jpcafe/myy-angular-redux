import angular from 'angular';
export default angular.module('app.filters.trustedUrl', [])
    .filter('trustedUrl', ['$sce', ($sce) => {
        return (url) => {
            return $sce.trustAsResourceUrl(url);
        };
    }]);