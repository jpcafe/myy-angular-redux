const initialState = {
    isLoading: false
};

export default (state = initialState, action) => {
    switch (action.type) {
        case 'SHOW_SPINNER':
            return Object.assign({}, state, {
                isLoading: true
            });
        case 'HIDE_SPINNER':
            return Object.assign({}, state, {
                isLoading: false
            });
        default:
            return state;
    }
};
