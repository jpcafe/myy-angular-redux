import {
    combineReducers
} from 'redux';
import home from './home-reducer';
import app from './app-reducer';

const rootReducer = combineReducers({
    home,
    app
});

export default rootReducer;
