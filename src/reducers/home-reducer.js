import ArtistHelper from 'app/helpers/artist-helper-class';
import VideoHelper from 'app/helpers/video-helper-class';

const initialState = {
    videos: [],
    filteredVideos: [],
    selectedVideo: null,
    searchText: '',
    artists: []
};

export default (state = initialState, action) => {
    let videos;
    switch (action.type) {
        case 'SELECT_VIDEO':
            return Object.assign({}, state, {
                selectedVideo: action.payload.video
            });

        case 'SELECT_ARTIST':
            return Object.assign({}, state, {
                artists: ArtistHelper.selectArtist(action.payload.name, state.artists)
            });
        
        case 'FILTER_VIDEOS':
            return Object.assign({}, state, {
                filteredVideos: VideoHelper.filter(state.videos, state.artists, state.searchText)
            });

        case 'LOAD_VIDEOS_SUCCESS':
            videos = VideoHelper.buildList(action.payload.data.videos);
            return Object.assign({}, state, {
                videos,
                filteredVideos: videos,
                artists: ArtistHelper.buildList(videos)
            });

        case 'CHANGE_SEARCH_TEXT':
            return Object.assign({}, state, {
                searchText: action.payload.text
            });

        default:
            return state;
    }
};
