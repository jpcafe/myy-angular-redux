import angular from 'angular';
import component from './video-card.component';

const Module = angular.module('app.components.video-card', [])
    .component('videoCard', component);
export default Module;