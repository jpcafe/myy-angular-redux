import template from './video-card.html';
import controller from './video-card.controller';

const videoCardComponent = {
    template,
    controller,
    controllerAs: 'vm',
    bindings: {
        video: '<'
    }
};

export default videoCardComponent;