import angular from 'angular';
import component from './video-list.component';

const Module = angular.module('app.components.video-list', [])
    .component('videoList', component);

export default Module;