import template from './video-list.html';
import controller from './video-list.controller';

const videoListComponent = {
    template,
    controller,
    controllerAs: 'vm',
    bindings: {
        videos: '<',
        onVideoClick: '&'
    }
};

export default videoListComponent;