import VideoListModule from './video-list/video-list';
import VideoCardModule from './video-card/video-card';
import SideMenuModule from './side-menu/side-menu';

import angular from 'angular';

export default angular.module('app.components', [
    VideoListModule.name,
    VideoCardModule.name,
    SideMenuModule.name
]);
