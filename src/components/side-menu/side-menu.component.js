import template from './side-menu.html';
import controller from './side-menu.controller';

export default {
    template,
    controller,
    controllerAs: 'vm',
    bindings: {
        artists: '<',
        allSelected: '<',
        onSelectArtist: '&',
        searchText: '<',
        onSearchTextChange: '&'
    }
};
