import angular from 'angular';
import component from './side-menu.component';

export default angular.module('app.components.side-menu', [])
    .component('sideMenu', component);
