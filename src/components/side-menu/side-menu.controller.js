export default class SideMenuController {
    $onInit() {
        this.allSelected = true;
    }

    toggleCheckbox(name) {
        this.onSelectArtist({
            name
        });
        this.allSelected = this.checkAllSelected();
    }

    toggleAll() {
        this.artists
            .filter(a => a.selected !== this.allSelected)
            .forEach(a => this.onSelectArtist({
                name: a.name
            }));
    }

    checkAllSelected() {
        return this.artists.every(a => a.selected === true);
    }
}
