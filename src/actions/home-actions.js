export const selectVideo = (video) => {
    return {
        type: 'SELECT_VIDEO',
        payload: {
            video
        }
    };
};

export const loadVideos = (video) => {
    return (dispatch, getState, helpers) => {
        const promise = helpers.api.videoService.getVideos()
            .then((data) => {
                return data;
            });

        dispatch({
            type: 'LOAD_VIDEOS',
            payload: {
                promise
            }
        });
    };
};

export const selectArtist = (name) => {
    return (dispatch, getState, helpers) => {
        dispatch({
            type: 'SELECT_ARTIST',
            payload: {
                name
            }
        });
        dispatch(filterVideos());
    };
};

export const changeSearchText = (text) => {
    return (dispatch, getState, helpers) => {
        dispatch({
            type: 'CHANGE_SEARCH_TEXT',
            payload: {
                text
            }
        });
        dispatch(filterVideos());
    };
};

export const filterVideos = () => {
    return {
        type: 'FILTER_VIDEOS'
    };
};

