import angular from 'angular';
import angularRoute from 'angular-route';
import createLogger from 'redux-logger';
import {
    combineReducers
} from 'redux';
import thunk from 'redux-thunk';
import ngRedux from 'ng-redux';
import ngSanitize from 'angular-sanitize';
import angularMaterial from 'angular-material';
import promiseMiddleware from 'redux-promise-middleware';

import appComponent from './app.component';
import 'app/stylesheets';
import 'app/containers';
import 'app/components';
import 'app/filters';
import 'app/services';
import rootReducer from 'app/reducers';

const logger = createLogger({
    level: 'info',
    collapsed: true
});

const promises = promiseMiddleware({
    promiseTypeSuffixes: ['PENDING', 'SUCCESS', 'ERROR']
});

let apiWrapper = {
    api: {}
};

angular.module('app', [ngSanitize, ngRedux, angularMaterial, 'ngRoute', 'app.services', 'app.filters', 'app.containers', 'app.components'])
    .run((VideoService) => {
        apiWrapper.api.videoService = VideoService;
    })
    .config(($routeProvider) => {
        $routeProvider
            .when("/", {
                template: "<home-container></home-container>"
            })
            .otherwise({
                template: "<h1>Not Found</h1>"
            });
        console.log('Router ready!');
    })
    .config(($ngReduxProvider) => {
        $ngReduxProvider.createStoreWith(rootReducer, [thunk.withExtraArgument(apiWrapper), promises, logger]);
        console.log('Store ready!');
    })
    .config(($mdThemingProvider) => {
        $mdThemingProvider
            .theme('default')
            .primaryPalette('blue')
            .accentPalette('teal')
            .warnPalette('red')
            .backgroundPalette('grey');
    })
    .component('app', appComponent);

console.log('Started app!');
