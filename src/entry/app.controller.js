class AppController {
    constructor($ngRedux) {
        this.unsubscribe = $ngRedux.connect(
                this.mapStateToThis)
            (this);
    }

    mapStateToThis(state) {
        return {
            isLoading: state.app.isLoading
        }
    }

    $onDestroy() {
        this.unsubscribe();
    }
}

export default AppController;
