import controller from './app.controller';
import template from './app.html';

const appComponent = {
    template,
    controller,
    controllerAs: 'vm'
};

export default appComponent;
