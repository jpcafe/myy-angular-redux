import angular from 'angular';
import * as AppActions from 'app/actions/app-actions';

class HttpService {
    constructor($http, $ngRedux) {
        this.$ngRedux = $ngRedux;
        this.$http = $http;
        this.baseUrl = 'http://localhost:3000/api';
        this.headers = {
            'Content-Type': 'application/json'
        };
    }

    showSpinner() {
        this.$ngRedux.dispatch(AppActions.showSpinner());
    }

    hideSpinner() {
        this.$ngRedux.dispatch(AppActions.hideSpinner());
    }

    query({
        data = undefined,
        route = '',
        method = 'GET'
    } = {}) {
        this.showSpinner();

        return this.$http({
                data,
                headers: this.headers,
                url: this.getUrl(route),
                method
            })
            .then((res) => {
                this.hideSpinner();
                return {
                    data: res.data,
                    status: res.status
                };
            })
            .catch(() => {
                this.hideSpinner();
            });
    }

    getUrl(route) {
        return `${this.baseUrl}/${route}`;
    }
}

export default angular.module('app.services.http-service', [])
    .service('HttpService', HttpService);
