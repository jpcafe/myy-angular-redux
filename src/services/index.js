import angular from 'angular';
import videoService from './video.service';
import httpService from './http.service';

export default angular.module('app.services', [
    videoService.name,
    httpService.name
]);
