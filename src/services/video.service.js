import angular from 'angular';

class VideoService {
    constructor(HttpService) {
        this.httpService = HttpService;
    }
    getVideos() {
        return this.httpService.query({
            route: 'videos'
        });
    }
}

export default angular.module('app.services.video-service', [])
    .service('VideoService', VideoService);
